---
title: "care management solutions"
date: 2021-01-19T18:27:04-05:00
draft: true
---

OpenEMR support and medical billing specialists since 1982 featuring 2 AAPC certified coders.

[![Certified OpenEMR contributor](/120px-Cert_op_cont_V3.png)](https://open-emr.org)

ask us to take your legacy version from

![Legacy](/legacy_logo.gif)

to the latest release

![Latest Release](/version6-1.png)

[email us](mailto:stephen.waite@cmsvt.com) or call 800-371-8685
